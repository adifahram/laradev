FROM php:8.1-apache

ARG UID
ARG GID

ENV UID=${UID}
ENV GID=${GID}

RUN addgroup --gid ${GID} --system developer
RUN adduser --system --shell /bin/bash --uid ${UID} --gid ${GID} developer

WORKDIR /var/www/html

ENV APACHE_DOCUMENT_ROOT /var/www/html/public


RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN chown -R www-data:www-data /var/www/html

RUN docker-php-ext-install pdo pdo_mysql

USER developer